"""
- CS2911 - 011
- Fall 2017
- Lab 8
- Names:
  - Nick Roth
  - Josh Svehla

A simple email sending program.

Thanks to Trip Horbinski from the Fall 2015 class for providing the password-entering functionality.
"""

# GUI library for password entry
import tkinter as tk

# Socket library
import socket

# SSL/TLS library
import ssl

# base-64 encode/decode
import base64

# Python date/time and timezone modules
import datetime
import time
import pytz
import tzlocal

# Module for reading password from console without echoing it
import getpass

# Modules for some file operations
import os
import mimetypes

# Host name for MSOE (hosted) SMTP server
SMTP_SERVER = 'smtp.office365.com'

# The default port for STARTTLS SMTP servers is 587
SMTP_PORT = 587

# SMTP domain name
SMTP_DOMAINNAME = 'msoe.edu'


def main():
    """Main test method to send an SMTP email message.

    Modify data as needed/desired to test your code,
    but keep the same interface for the smtp_send
    method.
    """
    (username, password) = login_gui()

    message_info = {}
    # message_info['To'] = 'sebern@msoe.edu'
    message_info['To'] = 'yoder@msoe.edu'
    message_info['From'] = username
    message_info['Subject'] = 'Yet another test message'
    message_info['Date'] = 'Thu, 9 Oct 2014 23:56:09 +0000'
    message_info['Date'] = get_formatted_date()

    print("message_info =", message_info)

    message_text = 'Test message_info number 6\r\n\r\nAnother line.'

    smtp_send(password, message_info, message_text)


def login_gui():
    """
    Creates a graphical user interface for secure user authorization.

    :return: (email_value, password_value)
        email_value -- The email address as a string.
        password_value -- The password as a string.

    :author: Tripp Horbinski
    """
    gui = tk.Tk()
    gui.title("MSOE Email Client")
    center_gui_on_screen(gui, 370, 120)

    tk.Label(gui, text="Please enter your MSOE credentials below:") \
        .grid(row=0, columnspan=2)
    tk.Label(gui, text="Email Address: ").grid(row=1)
    tk.Label(gui, text="Password:         ").grid(row=2)

    email = tk.StringVar()
    email_input = tk.Entry(gui, textvariable=email)
    email_input.grid(row=1, column=1)

    password = tk.StringVar()
    password_input = tk.Entry(gui, textvariable=password, show='*')
    password_input.grid(row=2, column=1)

    auth_button = tk.Button(gui, text="Authenticate", width=25, command=gui.destroy)
    auth_button.grid(row=3, column=1)

    gui.mainloop()

    email_value = email.get()
    password_value = password.get()

    return email_value, password_value


def center_gui_on_screen(gui, gui_width, gui_height):
    """Centers the graphical user interface on the screen.

    :param gui: The graphical user interface to be centered.
    :param gui_width: The width of the graphical user interface.
    :param gui_height: The height of the graphical user interface.
    :return: The graphical user interface coordinates for the center of the screen.
    :author: Tripp Horbinski
    """
    screen_width = gui.winfo_screenwidth()
    screen_height = gui.winfo_screenheight()
    x_coord = (screen_width / 2) - (gui_width / 2)
    y_coord = (screen_height / 2) - (gui_height / 2)

    return gui.geometry('%dx%d+%d+%d' % (gui_width, gui_height, x_coord, y_coord))


# *** Do not modify code above this line ***


def smtp_send(password, message_info, message_text):
    """Send a message via SMTP.

    :param password: String containing user password.
    :param message_info: Dictionary with string values for the following keys:
                'To': Recipient address (only one recipient required)
                'From': Sender address
                'Date': Date string for current date/time in SMTP format
                'Subject': Email subject
            Other keys can be added to support other email headers, etc.
    :author Josh Svehla
    """
    data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    data_socket.connect((SMTP_SERVER, SMTP_PORT))
    if not send_initialize(data_socket):
        return
    context = ssl.create_default_context()
    wrapped_socket = context.wrap_socket(data_socket, server_hostname=SMTP_SERVER)
    send_encrypted(wrapped_socket, message_info, message_text, password)
    wrapped_socket.close()
    data_socket.close()


def send_initialize(data_socket):
    """
    Sends and receives the messages to initialize the encrypted connection
    :param data_socket: The socket to send/receive through
    :return true if successful, else false
    :author Josh Svehla
    """

    if read_code(data_socket)[:-1] != '220':
        print('Server not ready: Option code not 220')
        return False
    ready_message = read_remainder(data_socket)
    data_socket.send(('EHLO ' + SMTP_DOMAINNAME + '\r\n').encode())
    initial_options = read_options(data_socket)
    if not initial_options:
        print('System Error: Option code not 250')
        return False
    data_socket.send('STARTTLS\r\n'.encode())
    if read_code(data_socket)[:-1] != '220':
        print('Server not ready: Option code not 220')
        return False
    read_remainder(data_socket)
    return True


def read_options(receiving_socket):
    """
    Uses the socket to read in all the options given by the server
    :param receiving_socket: The socket to receive the lines from
    :return: A list containing all the options (in case someone were to need the info)
    :author Josh Svehla
    """

    options = []
    code = read_code(receiving_socket)
    option = read_remainder(receiving_socket)
    while code[-1] != ' ':
        code = code[:-1]
        if code != '250':
            return False
        options.append(option)
        code = read_code(receiving_socket)
        option = read_remainder(receiving_socket)
    options.append(option)
    return options


def read_code(data_socket):
    """
    Reads the code before an option from the server. Includes either the space or '-' so the
    calling method know when to stop
    :param data_socket: The socket to read from
    :return: The code before the option
    :author Josh Svehla
    """

    code = ''
    current_char = ''
    while current_char != ' ' and current_char != '-':
        current_char = data_socket.recv(1).decode()
        code += current_char
    return code


def read_remainder(data_socket):
    """
    Reads the remainder of the line after the code from the server
    :param data_socket: The socket to read from
    :return: The remainder of the line sent by the server
    :author Josh Svehla
    """

    remainder = ''
    current_char = data_socket.recv(1).decode()
    while current_char != '\r':
        remainder += current_char
        current_char = data_socket.recv(1).decode()
    # Receive the extra \n
    data_socket.recv(1)
    return remainder


def send_encrypted(wrapped_socket, message_info, message_text, password):
    """
    Compiles all the SMTP requests to make and then passes them to the looping helper method
    :param wrapped_socket: The secure TLS/SSL connection to use to send the email
    :param message_info: The headers of the email
    :param message_text: The body of the email
    :param password: The password of the user email
    :return: None
    :author: Nick Roth
    """

    send = {}
    send[0] = ('EHLO ' + SMTP_DOMAINNAME + '\r\n').encode()
    send[1] = ('AUTH LOGIN' + '\r\n').encode()
    send[2] = (base64.b64encode((message_info["From"]).encode())) + b'\r\n'
    send[3] = (base64.b64encode(password.encode())) + b'\r\n'
    send[4] = ("MAIL FROM: <" + message_info["From"] + ">\r\n").encode()
    send[5] = ("RCPT TO: <" + message_info["To"] + ">\r\n").encode()
    send[6] = "DATA\r\n".encode()
    send[7] = ("From: Josh and Nick <" + message_info["From"] + ">\r\nTo: Nicholas Roth <" + message_info["To"] +
               ">\r\nSubject: " + message_info["Subject"] + "\r\nDate: " + message_info["Date"] +
               "\r\n\r\n" + message_text + "\r\n.\r\n").encode()
    send[8] = "QUIT\r\n".encode()
    encrypted_send_receive_loop(wrapped_socket, send)


def encrypted_send_receive_loop(wrapped_socket, send):
    """
    Given all the SMTP requests to make, handles sending the request and reading and printing the responses.
    :param wrapped_socket: The socket to send to
    :param send: The table of all the SMTP requests to make
    :return: None
    :author: Nick Roth
    """

    for i in range(0, len(send)):
        print("U> " + str(send[i].decode()))
        wrapped_socket.send(send[i])
        read_response(wrapped_socket)


def read_response(wrapped_socket):
    """
    Reads the response from the server until there is no more to read
    :param wrapped_socket: The socket to read from
    :return: None
    :author: Nick Roth
    """

    response = ''
    current_char = wrapped_socket.recv(1).decode()
    while current_char != '\r':
        response += current_char
        current_char = wrapped_socket.recv(1).decode()
    wrapped_socket.recv(1)  # \r\n_ <-- Move pointer past \n
    print("S> " + str(response))
    if response[3] == '-':
        read_response(wrapped_socket)  # Repeat this method if there are more responses to read
    return


# ** Do not modify code below this line. **

# Utility functions
# You may use these functions to simplify your code.


def get_formatted_date():
    """Get the current date and time, in a format suitable for an email date header.

    The constant TIMEZONE_NAME should be one of the standard pytz timezone names.
    If you really want to see them all, call the print_all_timezones function.

    tzlocal suggested by http://stackoverflow.com/a/3168394/1048186

    See RFC 5322 for details about what the timezone should be
    https://tools.ietf.org/html/rfc5322

    :return: Formatted current date/time value, as a string.
    """
    zone = tzlocal.get_localzone()
    print("zone =", zone)
    timestamp = datetime.datetime.now(zone)
    timestring = timestamp.strftime('%a, %d %b %Y %H:%M:%S %z')  # Sun, 06 Nov 1994 08:49:37 +0000
    return timestring


def print_all_timezones():
    """ Print all pytz timezone strings. """
    for tz in pytz.all_timezones:
        print(tz)


# You probably won't need the following methods, unless you decide to
# try to handle email attachments or send multi-part messages.
# These advanced capabilities are not required for the lab assignment.


def get_mime_type(file_path):
    """Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    :param file_path: String containing path to (resource) file, such as './abc.jpg'
    :return: If successful in guessing the MIME type, a string representing the content
             type, such as 'image/jpeg'
             Otherwise, None
    :rtype: int or None
    """

    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """Try to get the size of a file (resource) in bytes, given its path

    :param file_path: String containing path to (resource) file, such as './abc.html'

    :return: If file_path designates a normal file, an integer value representing the the file size in bytes
             Otherwise (no such file, or path is not a file), None
    :rtype: int or None
    """

    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()
